package com.egegen.sahelapp.fragments;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.FragmentBase;
import com.egegen.sahelapp.models.Road;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class FragmentRoadReport extends FragmentBase implements View.OnClickListener {

    private String address = "--";
    private double latitude = 0.1;
    private double longitude = 0.1;

    private ImageView img_road_work, img_obstruction, img_broken, img_ramps;
    private boolean img_1=false, img_2=false, img_3=false, img_4=false;

    private Road point;

    private Button btn_report;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_road_report, container, false);

        img_road_work   = v.findViewById(R.id.img_road_work);
        img_obstruction = v.findViewById(R.id.img_obstruction);
        img_broken      = v.findViewById(R.id.img_broken);
        img_ramps       = v.findViewById(R.id.img_ramps);
        btn_report      = v.findViewById(R.id.btn_report);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference();

        img_broken.setOnClickListener(this);
        img_road_work.setOnClickListener(this);
        img_obstruction.setOnClickListener(this);
        img_ramps.setOnClickListener(this);

        Intent intent = getActivity().getIntent();
        latitude = intent.getDoubleExtra("latitude",0.1);

        longitude = intent.getDoubleExtra("longitude",0.1);
        address = intent.getStringExtra("address");

        point = new Road(latitude,longitude,address,false,false,false,false, currentUser.getUid());

        btn_report.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_road_work:

               if (!img_1) {

                    img_1 = true;
                    img_road_work.setBackgroundResource(R.drawable.mystroke);
                    point.setRoadwork(true);
                } else {
                   Toast.makeText(getActivity(), "YYY", Toast.LENGTH_SHORT).show();
                    img_1 = false;
                    img_road_work.setBackgroundResource(R.drawable.roads_menu_bg);
                    point.setRoadwork(false);
                }
                break;
            case R.id.img_obstruction:

                if (!img_2) {
                    img_2 = true;
                    img_obstruction.setBackgroundResource(R.drawable.mystroke);
                    point.setObstruction(true);
                } else {
                    img_2 = false;
                    img_obstruction.setBackgroundResource(R.drawable.roads_menu_bg);
                    point.setObstruction(false);
                }

                break;
            case R.id.img_broken:
                if (!img_3) {
                    img_3 = true;
                    img_broken.setBackgroundResource(R.drawable.mystroke);
                    point.setBroken(true);
                } else {
                    img_3 = false;
                    img_broken.setBackgroundResource(R.drawable.roads_menu_bg);
                    point.setBroken(false);
                }
                break;
            case R.id.img_ramps:
                if (!img_4) {
                    img_4 = true;
                    img_ramps.setBackgroundResource(R.drawable.mystroke);
                    point.setRamps(true);
                } else {
                    img_4 = false;
                    img_ramps.setBackgroundResource(R.drawable.roads_menu_bg);
                    point.setRamps(false);
                }
                break;

            case R.id.btn_report:
                pointSaveDatabase(point);
                break;
        }
    }

    private void pointSaveDatabase(Road point){

        String key = dbRef.child("RoadsReports").push().getKey();
        dbRef.child("RoadsReports").child(key).setValue(point).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
