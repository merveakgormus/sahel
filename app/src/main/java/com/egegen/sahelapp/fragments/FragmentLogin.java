package com.egegen.sahelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.activity.MapsActivity;
import com.egegen.sahelapp.base.FragmentBase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FragmentLogin extends FragmentBase implements View.OnClickListener {

    private EditText edt_mail, edt_password;
    private Button btn_sign_in;
    private TextView tv_go_register;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        edt_mail        = v.findViewById(R.id.edt_email);
        edt_password    = v.findViewById(R.id.edt_password);
        btn_sign_in     = v.findViewById(R.id.btn_sign_in);
        tv_go_register  = v.findViewById(R.id.tv_go_register);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();

        if (currentUser != null){
            startActivity(new Intent(getActivity(), MapsActivity.class));
        }

        tv_go_register.setOnClickListener(this);
        btn_sign_in.setOnClickListener(this);

        return v;
    }

    public void SignUp(String email, String password){
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_in:
                    SignUp(edt_mail.getText().toString(), edt_password.getText().toString());
                break;
            case R.id.tv_go_register:
                    MyTransaction(new FragmentRegister(), R.id.frame_layout);
                break;
        }
    }
}
