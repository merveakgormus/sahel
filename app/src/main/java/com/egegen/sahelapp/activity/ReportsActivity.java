package com.egegen.sahelapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.BaseActivity;
import com.egegen.sahelapp.fragments.FragmentPlaceReport;
import com.egegen.sahelapp.fragments.FragmentRoadReport;

public class ReportsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        Intent intent = getIntent();
        if(intent.getIntExtra("page", 0) == 0){
            MyTransactionWithoudAnim(new FragmentRoadReport(), R.id.road_frame_layout);
        }else if(intent.getIntExtra("page", 0) == 1){
            MyTransactionWithoudAnim(new FragmentPlaceReport(), R.id.road_frame_layout);
        }

    }
}
