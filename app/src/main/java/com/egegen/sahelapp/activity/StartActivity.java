package com.egegen.sahelapp.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.BaseActivity;
import com.egegen.sahelapp.fragments.FragmentLogin;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        MyTransaction(new FragmentLogin(), R.id.frame_layout);

    }
}
