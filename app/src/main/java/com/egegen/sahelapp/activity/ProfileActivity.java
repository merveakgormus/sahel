package com.egegen.sahelapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.BaseActivity;
import com.egegen.sahelapp.fragments.FragmentProfile;

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        MyTransaction(new FragmentProfile(), R.id.profile_frame_layout);
    }
}
