package com.egegen.sahelapp.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.FragmentBase;
import com.egegen.sahelapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FragmentProfileEdit extends FragmentBase implements View.OnClickListener {


    EditText edtName, edtSurname, edtCountry, edtCity, edtEmail, edtPhone, edtFullName;
    Button btnSave;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());

        edtName     = v.findViewById(R.id.edtName);
        edtSurname  = v.findViewById(R.id.edtSurname);
        edtCountry  = v.findViewById(R.id.edtCountry);
        edtCity     = v.findViewById(R.id.edtCity);
        edtEmail    = v.findViewById(R.id.edtEmail);
        edtPhone    = v.findViewById(R.id.edtPhone);
        edtFullName = v.findViewById(R.id.edtFullName);
        btnSave     = v.findViewById(R.id.btnSave);

        btnSave.setOnClickListener(this);
        edtEmail.setText(currentUser.getEmail());
        edtEmail.setEnabled(false);

        Log.d("EMAIL",""+currentUser.getUid());
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                edtName.setText(dataSnapshot.child("name").getValue().toString());
                edtSurname.setText(dataSnapshot.child("surname").getValue().toString());
                edtCountry.setText(dataSnapshot.child("country").getValue().toString());
                edtCity.setText(dataSnapshot.child("city").getValue().toString());
                edtPhone.setText(dataSnapshot.child("phone").getValue().toString());
                edtFullName.setText(dataSnapshot.child("fullname").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return v;
    }

    private void UpdateProfileData(User u){

        dbRef.setValue(u);
        getActivity().onBackPressed();

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                //String name, String surname, String country, String city, String email, String phone, String fullname
                User u = new User(edtName.getText().toString(), edtSurname.getText().toString(), edtCountry.getText().toString(), edtCity.getText().toString(), edtEmail.getText().toString(), edtPhone.getText().toString(), edtFullName.getText().toString());
                UpdateProfileData(u);
                break;
        }
    }
}
