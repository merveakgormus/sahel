package com.egegen.sahelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.activity.MapsActivity;
import com.egegen.sahelapp.base.FragmentBase;
import com.egegen.sahelapp.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class FragmentRegister extends FragmentBase implements View.OnClickListener {

    Button btn_sign_up;
    EditText edt_name, edt_surname, edt_email, edt_password, edt_confirm_password;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container,false);

        btn_sign_up     = v.findViewById(R.id.btn_sign_up);
        edt_name        = v.findViewById(R.id.edt_name);
        edt_surname     = v.findViewById(R.id.edt_surname);
        edt_email       = v.findViewById(R.id.edt_email);
        edt_password    = v.findViewById(R.id.edt_password);
        edt_confirm_password = v.findViewById(R.id.edt_confirm_password);
        
        btn_sign_up.setOnClickListener(this);


        auth = FirebaseAuth.getInstance();
        auth.signOut();
        auth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference("Users");

        return v;
    }

    private void SignUp(final User u, String password){
        auth.createUserWithEmailAndPassword(u.getEmail(), password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    FirebaseUser currentUser1 = mAuth.getCurrentUser();

                    // task.getResult()
                    dbRef.child(currentUser1.getUid()).setValue(u);
                    startActivity(new Intent(getActivity(), MapsActivity.class));
                    Toast.makeText(getActivity(), "User registration created successfully.", Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_up:

                String name = "", surname = "", country = "", city = "", email = "", phone = "", fullname = "";
                name = edt_name.getText().toString();
                surname = edt_surname.getText().toString();
                email = edt_email.getText().toString();
                //String name, String surname, String country, String city, String email, String phone, String fullname
                User  u = new User(name, surname,country,city, email, phone, fullname);
                SignUp(u, edt_password.getText().toString());

                break;
        }
    }
}
