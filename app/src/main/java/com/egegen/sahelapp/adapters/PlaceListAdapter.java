package com.egegen.sahelapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.models.Place;

import java.util.List;

public class PlaceListAdapter extends BaseAdapter {

    List<Place> placeList;
    Activity context;

    public PlaceListAdapter(List<Place> placeList, Activity context) {
        this.placeList = placeList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return placeList.size();
    }

    @Override
    public Object getItem(int i) {
        return placeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = context.getLayoutInflater();
        View v = inflater.inflate(R.layout.list_view_item_places, null, false);
        TextView tv_place_name = v.findViewById(R.id.tv_place_name);
        TextView tv_place_address = v.findViewById(R.id.tv_place_address);
        Button btn_raiting = v.findViewById(R.id.btn_raiting);

        tv_place_name.setText(placeList.get(i).getPlaceName());
        double toplam = placeList.get(i).getParkingRaiting()+placeList.get(i).getElevatorRaiting()+placeList.get(i).getEntranceRaiting()+placeList.get(i).getToiletRaiting();
        btn_raiting.setText((toplam/4)+"");

        return v;
    }
}
