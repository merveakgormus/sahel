package com.egegen.sahelapp.activity;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.BaseActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,View.OnClickListener, GoogleMap.OnPoiClickListener {

    private GoogleMap mMap;

    public FirebaseAuth auth;
    public FirebaseUser currentUser;
    public DatabaseReference dbRef;

    private final LatLng mDefaultLocation = new LatLng(26.390662, 55.470130);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;

    private CameraPosition mCameraPosition;

    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";


    private LatLng reportLatlng = null;
    private String reportAddress = null;

    private String placeName, placeId;
    private double placeLatitude, placeLongitude;

    private ArrayList<Marker> mMarkerArray;

    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;

    private Button btn_report_roads, btn_report_places, btn_report_issue;
    private View custom_app_bar;
    private ImageView go_menu, go_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference("RoadsReports");

        if (currentUser == null){
            startActivity( new Intent(MapsActivity.this, StartActivity.class));
            this.finish();
        }

        getReportedPoints();

        mMarkerArray = new ArrayList<>();

        custom_app_bar = findViewById(R.id.custom_app_bar);
        go_profile     = custom_app_bar.findViewById(R.id.go_profile);

        btn_report_issue    = findViewById(R.id.btn_report_issue);
        btn_report_places   = findViewById(R.id.btn_report_places);
        btn_report_roads    = findViewById(R.id.btn_report_roads);

        btn_report_issue.setOnClickListener(this);
        btn_report_places.setOnClickListener(this);
        btn_report_roads.setOnClickListener(this);

        mGeoDataClient = Places.getGeoDataClient(this, null);
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //Mevcut ekli işletmelerin bilgisini verir !!
        mMap.setOnPoiClickListener(this);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                for (int i =0; i< mMarkerArray.size(); i++){
                    if (mMarkerArray.get(i).getTitle().contains("SelectedRoad")){
                        mMarkerArray.get(i).remove();
                    }
                }

                Marker newMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("SelectedRoad"));

                reportLatlng = latLng;

                mMarkerArray.add(newMarker);

                Geocoder geoCoder = new Geocoder(MapsActivity.this);
                try {

                    List<Address> matches = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
                    reportAddress = bestMatch.getAddressLine(0);


                } catch (Exception e) {
                    Log.d("Bir hata oluştu.", e.toString());
                }
            }
        });



        getLocationPermission();
        getDeviceLocation();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_report_roads:
                Intent i = new Intent(MapsActivity.this, ReportsActivity.class);
                if (reportLatlng != null){
                    i.putExtra("page",0);
                    i.putExtra("latitude",reportLatlng.latitude);
                    i.putExtra("longitude",reportLatlng.longitude);
                    i.putExtra("address", reportAddress);
                    startActivity(i);
                }else {
                    Toast.makeText(this, "Please select coordinates!", Toast.LENGTH_SHORT).show();
                }


                break;

            case R.id.btn_report_places:

                if(placeId != null){
                    Intent intent = new Intent(MapsActivity.this, ReportsActivity.class);
                    intent.putExtra("page",1);
                    intent.putExtra("lat",placeLatitude);
                    intent.putExtra("long",placeLongitude);
                    intent.putExtra("placeName", placeName);
                    intent.putExtra("placeId", placeId);

                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Please select places!", Toast.LENGTH_SHORT).show();
                }
                break;

            case  R.id.btn_report_issue:
                if (btn_report_places.getVisibility() == View.VISIBLE){
                    btn_report_places.setVisibility(View.GONE);
                    btn_report_roads.setVisibility(View.GONE);
                }else{
                    btn_report_places.setVisibility(View.VISIBLE);
                    btn_report_roads.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.go_profile:
                startActivity(new Intent(MapsActivity.this, ProfileActivity.class));
                break;
        }
    }


    @Override
    public void onPoiClick(PointOfInterest poi) {
        Toast.makeText(getApplicationContext(), "Selected places: " + poi.name , Toast.LENGTH_SHORT).show();

        placeId = poi.placeId;
        placeLatitude = poi.latLng.latitude;
        placeLongitude = poi.latLng.longitude;
        placeName = poi.name;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    private void getDeviceLocation() {

        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {

                            mLastKnownLocation = task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                        } else {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getReportedPoints(){
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    double latitude = (double) ds.child("latitude").getValue();
                    double longitude = (double) ds.child("longitude").getValue();

                    String title="";

                    if((boolean)ds.child("broken").getValue()){
                        title = " Broken-Uneven";
                    }
                    if((boolean)ds.child("obstruction").getValue()){
                        title += " Obstruction";
                    }
                    if((boolean)ds.child("ramps").getValue()){
                        title += " Ramps";
                    }
                    if((boolean)ds.child("roadwork").getValue()){
                        title += " Road Work";
                    }

                    int height = 100;
                    int width = 100;

                    BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.pin);
                    Bitmap b=bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

                    LatLng ln = new LatLng(latitude, longitude);
                    mMap.addMarker(new MarkerOptions()
                            .position(ln)
                            .title(title)
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
