package com.egegen.sahelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.FragmentBase;
import com.egegen.sahelapp.models.Place;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class FragmentPlaceReport extends FragmentBase {

    private double latitude, longitude;
    private String placeId, placeName;

    private RatingBar ratingToilet, ratingEntr, ratingElevator, ratingParking;

    private Button btnReport;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place_report, container, false);
        Intent i = getActivity().getIntent();

        latitude    = i.getDoubleExtra("lat", 0.1);
        longitude   = i.getDoubleExtra("long", 0.1);
        placeName   = i.getStringExtra("placeName");
        placeId     = i.getStringExtra("placeId");

       // Toast.makeText(getActivity(), ""+latitude+ longitude+ placeName+placeId, Toast.LENGTH_SHORT).show();
        //String placeName, String placeId, double toiletRaiting, double entranceRaiting, double elevatorRaiting, double parkingRaiting, double latitude, double longitude
        ratingToilet    = v.findViewById(R.id.ratingToilet);
        ratingParking   = v.findViewById(R.id.ratingParking);
        ratingElevator  = v.findViewById(R.id.ratingElevator);
        ratingEntr      = v.findViewById(R.id.ratingEntrance);
        btnReport       = v.findViewById(R.id.btnReport);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference("PlacesReports");

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Place p = new Place(placeName, placeId, ratingToilet.getRating(), ratingEntr.getRating(), ratingElevator.getRating(), ratingParking.getRating(), latitude, longitude);
                PlaceReport(p);
                Toast.makeText(getActivity(), "" +ratingToilet.getRating(), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    private void PlaceReport(Place place){

        String key = dbRef.push().getKey();
        dbRef.child(key).setValue(place).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
