package com.egegen.sahelapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.base.FragmentBase;

public class FragmentProfile extends FragmentBase implements View.OnClickListener {

    Button btn_edit_profile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        btn_edit_profile = v.findViewById(R.id.btn_edit_profile);
        btn_edit_profile.setOnClickListener(this);
        return  v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_edit_profile:
                    MyTransaction(new FragmentProfileEdit(), R.id.profile_frame_layout);
                break;
        }
    }
}
