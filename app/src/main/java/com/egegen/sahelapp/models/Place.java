package com.egegen.sahelapp.models;

public class Place {

    private String placeName;
    private String placeId;
    private double toiletRaiting;
    private double entranceRaiting;
    private double elevatorRaiting;
    private double parkingRaiting;
    private double latitude;
    private double longitude;

    public Place(String placeName, String placeId, double toiletRaiting, double entranceRaiting, double elevatorRaiting, double parkingRaiting, double latitude, double longitude) {
        this.placeName = placeName;
        this.placeId = placeId;
        this.toiletRaiting = toiletRaiting;
        this.entranceRaiting = entranceRaiting;
        this.elevatorRaiting = elevatorRaiting;
        this.parkingRaiting = parkingRaiting;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getToiletRaiting() {
        return toiletRaiting;
    }

    public void setToiletRaiting(double toiletRaiting) {
        this.toiletRaiting = toiletRaiting;
    }

    public double getEntranceRaiting() {
        return entranceRaiting;
    }

    public void setEntranceRaiting(double entranceRaiting) {
        this.entranceRaiting = entranceRaiting;
    }

    public double getElevatorRaiting() {
        return elevatorRaiting;
    }

    public void setElevatorRaiting(double elevatorRaiting) {
        this.elevatorRaiting = elevatorRaiting;
    }

    public double getParkingRaiting() {
        return parkingRaiting;
    }

    public void setParkingRaiting(double parkingRaiting) {
        this.parkingRaiting = parkingRaiting;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



}
