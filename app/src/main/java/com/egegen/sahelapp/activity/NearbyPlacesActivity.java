package com.egegen.sahelapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.egegen.sahelapp.R;
import com.egegen.sahelapp.adapters.PlaceListAdapter;
import com.egegen.sahelapp.base.BaseActivity;
import com.egegen.sahelapp.models.Place;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class NearbyPlacesActivity extends BaseActivity {

    private ListView placesListView;
    private List<Place> placeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_places);

        placeList = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        dbRef = FirebaseDatabase.getInstance().getReference("PlacesReports");


        placesListView = findViewById(R.id.placesListV);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds :dataSnapshot.getChildren()){
                    //String placeName, String placeId, double toiletRaiting,
                    // double entranceRaiting,
                    //
                    // double elevatorRaiting,
                    // double parkingRaiting, double latitude, double longitude
                    Place p =
                            new Place(ds.child("placeName").getValue().toString(),
                                    ds.child("placeId").getValue().toString(),
                                    (double)ds.child("toiletRaiting").getValue(),
                                    (double)ds.child("entranceRaiting").getValue(),
                                    (double)ds.child("elevatorRaiting").getValue(),
                                    (double)ds.child("parkingRaiting").getValue(),
                                    (double)ds.child("latitude").getValue(),
                                    (double)ds.child("longitude").getValue());




                    placeList.add(p);
                }

                PlaceListAdapter adapter = new PlaceListAdapter(placeList, NearbyPlacesActivity.this);
                placesListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





    }
}
