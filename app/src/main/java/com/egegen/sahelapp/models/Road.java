package com.egegen.sahelapp.models;

public class Road {
    private double latitude;
    private double longitude;
    private String address;
    private boolean roadwork;
    private boolean obstruction;
    private boolean broken;
    private boolean ramps;
    private String uid;


    public Road(double latitude, double longitude, String address, boolean roadwork, boolean obstruction, boolean broken, boolean ramps, String uid) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.roadwork = roadwork;
        this.obstruction = obstruction;
        this.broken = broken;
        this.ramps = ramps;
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isRoadwork() {
        return roadwork;
    }

    public void setRoadwork(boolean roadwork) {
        this.roadwork = roadwork;
    }

    public boolean isObstruction() {
        return obstruction;
    }

    public void setObstruction(boolean obstruction) {
        this.obstruction = obstruction;
    }

    public boolean isBroken() {
        return broken;
    }

    public void setBroken(boolean broken) {
        this.broken = broken;
    }

    public boolean isRamps() {
        return ramps;
    }

    public void setRamps(boolean ramps) {
        this.ramps = ramps;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
